pragma solidity 0.8.10;
pragma abicoder v2;

contract MultiSigWallet {

    // deposits made by various addresses
    mapping(address => uint) deposits;
    
    // Array list of owners addresses
    address[] owners;
    uint approval_limit; // Limit of approvals before request is approved

    struct Request {
        uint id;
        address payable requested_by;
        uint amount;
        uint approved_num;
        bool withdrawn;
    }

    mapping(uint => mapping(address => bool)) _approvals;
    mapping(uint => Request) requests;

    modifier requestExists(uint id) {
        require(requests[id].amount > 0, "Request does not exists!");
        _;
    }

    modifier canApprove() {
        bool isOwner = false;
        for (uint i=0; i < owners.length; i++) {
            if(owners[i] == msg.sender) {
                isOwner = true;
            }
        }
        require(isOwner, "Must be owner to approve withdraw!");
        _;
    }

    event TransferRequest(uint request_id, address requested_by, uint transfer_amount);
    event RequestApproval(uint request_id, address approved_by);
    event RequestApprovedAndWithdrawn(uint request_id);

    constructor (address[] memory _owners, uint _approval_limit) {
        owners = _owners;
        approval_limit = _approval_limit;
    }

    // Get Owners of MultiSig
    function getOwners() public view returns (address[] memory){
        return owners;
    }

    // deposit ether to address
    function deposit() public payable {
        deposits[msg.sender] += msg.value;
    }

    function getContractBalance() public view returns (uint) {
        return address(this).balance;
    }

    // Get Request details
    function getRequest(uint id) public view requestExists(id) returns (Request memory) {
        return requests[id];
    }

    // Create new Withdraw Request
    function requestWithdraw(uint id, uint amount) public canApprove returns (uint){
        require(address(this).balance >= amount, "Cannot withdraw more than balance!");
        require(amount > 0, "Cannot withdraw zero!");

        Request memory newRequest = Request({
            id: id,
            requested_by: payable(msg.sender), 
            amount: amount, 
            approved_num: 0, 
            withdrawn: false
        });

        requests[id] = newRequest;
        emit TransferRequest(id, msg.sender, amount);

        return id;
    }

    // Approve Request for withdrawl
    function approveRequest(uint id) public canApprove requestExists(id) {
        require(requests[id].requested_by != msg.sender, "Cannot self approve!");
        require(_approvals[id][msg.sender] == false, "Already approved!");
        require(requests[id].withdrawn == false, "Already approved & withdrawn!");
        
        _approvals[id][msg.sender] = true;

        requests[id].approved_num += 1;

        emit RequestApproval(id, msg.sender);

        if(requests[id].approved_num >= approval_limit) {
            requests[id].withdrawn = true;
            (requests[id].requested_by).transfer(requests[id].amount);
            emit RequestApprovedAndWithdrawn(id);
        }
    }

}